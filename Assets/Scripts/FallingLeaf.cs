﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingLeaf : MonoBehaviour
{
    public float speed;
    public Vector3 direction;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        direction = new Vector3(1, -1, 0);
        transform.Translate(direction * Time.deltaTime * speed);
	}

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Background")
        {
            Destroy(this.gameObject);
        }
    }
}
