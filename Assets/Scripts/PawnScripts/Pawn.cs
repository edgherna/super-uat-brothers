﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour
{
    public Transform tf;
    public Vector3 homePosition;

    public float speed = 5f;
    public float jumpSpeed = 8f;
    public float movement = 0f;
    public Rigidbody2D rb;

	// Use this for initialization
	public virtual void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        homePosition = tf.position;
        movement = Input.GetAxis("Horizontal");
    }
	
	// Update is called once per frame
	public virtual void Update()
    {
		
	}

    public virtual void MoveLeft()
    {

    }

    public virtual void MoveRight()
    {

    }

    public virtual void Jump()
    {

    }
}