﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoxPawn : Pawn
{
	// Use this for initialization
	public override void Start()
    {
        base.Start();
	}
	
	// Update is called once per frame
	public override void Update()
    {
        base.Update();
	}

    public override void MoveLeft()
    {
        if (movement < 0f)
        {
            rb.velocity = new Vector2(movement * speed, rb.velocity.y);
        }
    }

    public override void MoveRight()
    {
        if (movement > 0f)
        {
            rb.velocity = new Vector2(movement * speed, rb.velocity.y);
        }
    }

    public override void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
    }
}
