﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFox : MonoBehaviour
{
/*
    The following below is to edit the player

    dirX is the direction of where the player is facing
    moveSpeed is how fast the fox is moving
    jumpForce is how high the fox will jump
*/
    public float moveSpeed, jumpForce;
    float dirX;
/*
    These are the components it needs for the sound and the animation.
    The following below is to give the player more components to make it feel like it is alive

    rb is the component that is needed for its physics
    animation is the current animation clip that is used at a certain time
    playerAudioClip is the list of the audio sounds for the player from which it 
    playerAudioSource
*/
    Rigidbody2D rb;
    Animator animation;
    public AudioClip[] playerAudioClip;
    AudioSource playerAudioSource;

/*
    facingRight is to check to see if the player is facing Right.
*/
    bool facingRight = true;
    Vector3 localScale;

    //The Camera component to follow the player.
    public GameObject playerCamera;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();                       //This is to get the physics components for the player.
        animation = GetComponent<Animator>();                   //This is to get the animation components for the player.
        playerAudioSource = GetComponent<AudioSource>();        //This is to get the sound components for the player.
        localScale = transform.localScale;                      //This is for the x-axis.

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Debug.Log("Game Ended!");
            Application.Quit();
        }

        if (Input.GetButtonDown("Jump") && rb.velocity.y == 0)
        {
            rb.AddForce(Vector2.up * jumpForce);
        }

        SetAnimationState();

        dirX = Input.GetAxisRaw("Horizontal") * moveSpeed;

        CheckPointManager.instance.UpdateCheckPoint();
        CameraFollow();
    }

    void FixedUpdate()
    {
        rb.velocity = new Vector2(dirX, rb.velocity.y);
    }

    void LateUpdate()
    {
        CheckWhereToFace();
    }

    void SetAnimationState()
    {
        //For the Idle Animation
        if (dirX == 0)
        {
            playerAudioSource.Stop();
            animation.SetBool("isWalking", false);
        }

        if (rb.velocity.y == 0)
        {
            animation.SetBool("isJumping", false);
            animation.SetBool("isFalling", false);
        }

        //For Moving Animation
        if (Mathf.Abs(dirX) == moveSpeed && rb.velocity.y == 0)
        {
            animation.SetBool("isWalking", true);
            if (!playerAudioSource.isPlaying)
            {
                playerAudioSource.clip = playerAudioClip[1];
                playerAudioSource.Play();
            }
        }

        //For Jumping Animation
        if (rb.velocity.y > 0)
        {
            animation.SetBool("isJumping", true);
            if (!playerAudioSource.isPlaying)
            {
                playerAudioSource.clip = playerAudioClip[0];
                playerAudioSource.Play();
            }
        }

        //For Falling Animation
        if (rb.velocity.y < 0)
        {
            animation.SetBool("isJumping", false);
            animation.SetBool("isFalling", true);
        }

    }

    void CheckWhereToFace()
    {
        if (dirX > 0)
        {
            facingRight = true;
        }
        else if (dirX < 0)
        {
            facingRight = false;
        }

        if (((facingRight) && (localScale.x < 0)) || ((!facingRight) && (localScale.x > 0)))
        {
            localScale.x *= -1;
        }

        transform.localScale = localScale;
    }

    public void PlaceAtPosition(Vector3 pos)
    {
        transform.position = pos;
        CameraFollow();
    }

    //This module will tell the camera to continue to follow the player as it moves throughout the level.
    void CameraFollow()
    {
        playerCamera.transform.position = new Vector3(transform.position.x, playerCamera.transform.position.y, playerCamera.transform.position.z);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            GameManager.instance.originPlayer.transform.position = new Vector3(GameManager.instance.origin.x,
                                                                               GameManager.instance.origin.y,
                                                                               GameManager.instance.origin.z);
            GameManager.instance.SetGameState((int)GameManager.GameState.GameOver);
        }

        if (other.gameObject.tag == "Win")
        {
            GameManager.instance.originPlayer.transform.position = new Vector3(GameManager.instance.origin.x,
                                                                               GameManager.instance.origin.y,
                                                                               GameManager.instance.origin.z);
            GameManager.instance.SetGameState((int)GameManager.GameState.Win);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Background")
        {
            GameManager.instance.originPlayer.transform.position = new Vector3(GameManager.instance.origin.x,
                                                                               GameManager.instance.origin.y,
                                                                               GameManager.instance.origin.z);
            GameManager.instance.SetGameState((int)GameManager.GameState.GameOver);
        }
    }
}
