﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafSpawner : MonoBehaviour
{
    //This is to control the spawning of the leaves of the level
    public GameObject leafPrefab;

    //This is to control the Spawn of the leaves
    float spawnDistance = 5f;
    public float leafRate = 5;
    public float nextLeaf = 1;

    static public int leafCount = 0;

	// Use this for initialization
	void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
        //Timer for when the leaves will spawn.
        nextLeaf -= Time.deltaTime;

        if (nextLeaf <= 0 /*&& leafCount < 3*/)
        {
            nextLeaf = leafRate;
            leafRate *= 0.9f;

            if (leafRate < 2)
            {
                leafRate = 2;
            }

            Vector3 offset = Random.insideUnitSphere;
            offset.z = 0;
            offset = offset.normalized * spawnDistance;
            Instantiate(leafPrefab, transform.position + offset, Quaternion.identity);
            leafCount++;
        }
	}
}
