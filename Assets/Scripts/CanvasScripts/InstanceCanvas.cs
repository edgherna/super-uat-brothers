﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanceCanvas : MonoBehaviour
{
    public static InstanceCanvas instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
