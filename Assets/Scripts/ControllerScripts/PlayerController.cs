﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller
{
	// Use this for initialization
	public override void Start()
    {
        base.Start();
	}
	
	// Update is called once per frame
	void Update()
    {
        //Fox's movement
		if (Input.GetKey(KeyCode.A))
        {
            pawn.MoveLeft();
        }

        if (Input.GetKey(KeyCode.D))
        {
            pawn.MoveRight();
        }

        //Fox's Jump
        if (Input.GetKeyDown(KeyCode.W))
        {
            pawn.Jump();
        }
	}
}