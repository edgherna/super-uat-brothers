﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour
{
    //The following below is used for the scene changes in the game.
    public static CheckPointManager instance;

    //
    public int checkpoint;
    public List<GameObject> checkPoints;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    private void Start()
    {
		for (int i = 0; i < checkPoints.Count; i++)
        {
            checkPoints[i].SetActive(false);
        }
	}

    public void SaveCheckPoint()
    {
        PlayerPrefs.SetInt("CheckPoint", checkpoint);
    }

    public void LoadCheckPoint()
    {
        checkpoint = PlayerPrefs.GetInt("Checkpoint", 0);
        GameManager.instance.player.PlaceAtPosition(checkPoints[checkpoint].transform.position);
    }

    public void Reset()
    {
        checkpoint = 0;
        SaveCheckPoint();
        GameManager.instance.player.PlaceAtPosition(checkPoints[checkpoint].transform.position);
    }

    public void UpdateCheckPoint()
    {
        for (int i = 0; i < checkPoints.Count; i++)
        {
            if (GameManager.instance.player.transform.position.x > checkPoints[i].transform.position.x)
            {
                if (i > checkpoint)
                {
                    checkpoint = i;
                    SaveCheckPoint();
                }
            }
        }
    }
}
