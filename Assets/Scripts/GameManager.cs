﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //List for the Canvas
    public GameObject mStartCanvas;
    public GameObject mLevelCanvas;
    public GameObject mMenuCanvas;
    public GameObject mWinCanvas;
    public GameObject mLoseCanvas;
    public Vector3 origin;

    bool menuState = false;

    public static GameManager instance;
    public GameObject originPlayer;
    public PlayerFox player;

    public enum GameState
    {
        Start,    //0
        Level,    //1
        Win,      //2
        GameOver, //3
        Menu      //4
    }

    public GameState gameState;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this);
        }
    }

    //Use this for initialization
    void Start()
    {
        CheckPointManager.instance.LoadCheckPoint();
        origin = new Vector3(instance.player.transform.position.x, instance.player.transform.position.y, instance.player.transform.position.z);
        SetGameState((int)GameState.Start);
	}
	
	//Update is called once per frame
	void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && menuState == true)
        {
            menuState = false;
            instance.mMenuCanvas.SetActive(false);
        }
        else if (Input.GetKey(KeyCode.Escape) && menuState == false)
        {
            menuState = true;
            instance.mMenuCanvas.SetActive(true);
        }
	}


    //When this module is called, it will change the canvas windows to open or close depending on the situation of the game
    void ActivateScreens()
    {
        switch(gameState)
        {
            case GameState.Start:
                mStartCanvas.SetActive(true);
                mLevelCanvas.SetActive(false);
                mWinCanvas.SetActive(false);
                mLoseCanvas.SetActive(false);
                break;

            case GameState.Level:
                mStartCanvas.SetActive(false);
                mLevelCanvas.SetActive(true);
                mWinCanvas.SetActive(false);
                mLoseCanvas.SetActive(false);
                break;

            case GameState.Win:
                mWinCanvas.SetActive(true);
                mStartCanvas.SetActive(false);
                mLevelCanvas.SetActive(false);
                mLoseCanvas.SetActive(false);
                break;

            case GameState.GameOver:
                mLoseCanvas.SetActive(true);
                mStartCanvas.SetActive(false);
                mLevelCanvas.SetActive(false);
                mWinCanvas.SetActive(false);
                break;
        }
    }

    //When this is called, it will set itself to the appropiate canvas status.
    public void SetGameState(int state)
    {
        gameState = (GameState)state;
        ActivateScreens();
    }

}